/*
  MIT License

  Copyright (c) 2024 Modding-OpenMW and Update Center Authors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

const updateListTable = document.getElementById("update-list");
const devListTable = document.getElementById("update-list-dev");
const gitlabBaseURL = "https://gitlab.com";
const gitlabAPIURL = `${gitlabBaseURL}/api/v4`;
const trackedMods = [
    {name: "Abandoned Flat", slug: "abandoned-flat"},
    {name: "Abandoned Flat Containers", slug: "abandoned-flat-containers"},
    {name: "Action Camera Swap", slug: "action-camera-swap"},
    {name: "An Nwahs Guide To Modern Culture And Mythology", slug: "an-nwahs-guide-to-modern-culture-and-mythology"},
    {name: "Black Soul Gems", slug: "black-soul-gems"},
    {name: "Bound Balance", slug: "bound-balance"},
    {name: "Convenient Thief Tools", slug: "convenient-thief-tools"},
    {name: "DeltaPlugin", slug: "delta-plugin", org: "bmwinger", url: "gitlab.com/bmwinger/delta-plugin#deltaplugin"},
    {name: "Distant Fixes: Lua Edition", slug: "distant-fixes-lua-edition"},
    {name: "Distant Fixes: Rise of House Telvanni", slug: "distant-fixes-rise-of-house-telvanni"},
    {name: "Distant Fixes: Uvirith's Legacy", slug: "distant-fixes-uviriths-legacy"},
    {name: "Distant Fixes: Uvirith's Manor", slug: "distant-fixes-uviriths-manor"},
    {name: "Dwemer Lightning Rods for OpenMW-Lua", slug: "dwemer-lightning-rods-for-openmw-lua"},
    {name: "Dynamic Telvanni Armor Replacer", slug: "dynamic-telvanni-armor-replacer"},
    {name: "Friendly Autosave", slug: "friendly-autosave"},
    {name: "Go Home!", slug: "go-home"},
    {name: "Gonzo's Scalable Dwemer Puzzle Box Randomizer", slug: "gonzos-scalable-dwemer-puzzle-box-randomizer"},
    {name: "Groundcoverify!", slug: "groundcoverify", org: "bmwinger", url: "gitlab.com/bmwinger/groundcoverify#groundcoverify"},
    {name: "Better Merchants Skills OpenMW", slug: "better-merchants-skills-openmw"},
    {name: "Harvest Lights", slug: "harvest-lights"},
    {name: "HD Forge for OpenMW-Lua", slug: "hd-forge-for-openmw-lua"},
    {name: "Light Hotkey", slug: "light-hotkey"},
    {name: "Mage Robes for OpenMW-Lua", slug: "mage-robes-for-openmw-lua"},
    {name: "Marksmans Eye", slug: "marksmans-eye"},
    {name: "Modding-OpenMW.com", slug: "modding-openmw.com", url: "modding-openmw.com", devBranch: "beta", devURL: "beta.modding-openmw.com"},
    {name: "MOMW Camera", slug: "momw-camera"},
    {name: "MOMW Configurator", slug: "momw-configurator"},
    {name: "MOMW Gameplay", slug: "momw-gameplay"},
    {name: "MOMW Patches", slug: "momw-patches"},
    {name: "MOMW Patches Graveyard", slug: "momw-patches-graveyard"},
    {name: "MOMW Post Processing Pack", slug: "momw-post-processing-pack"},
    {name: "MOMW Tools Pack", slug: "momw-tools-pack"},
    {name: "NCGDMW Lua Edition", slug: "ncgdmw-lua"},
    {name: "OpenMW Books Enhanced", slug: "openmw-books-enhanced"},
    {name: "NoPopUp Chargen", slug: "nopopup-chargen"},
    {name: "Oh No Stolen Reports", slug: "oh-no-stolen-reports"},
    {name: "omw", slug: "omw"},
    {name: "OpenMW-Validator", slug: "openmw-validator"},
    {name: "OpenNevermind", slug: "opennevermind"},
    {name: "Pause Control", slug: "pause-control"},
    {name: "Pharis' Magicka Regeneration", slug: "pharis-magicka-regeneration"},
    // {name: "Protected Beasts", slug: "protected-beasts"},
    {name: "Provincial Beginning", slug: "provincial-beginning"},
    {name: "Quickselect", slug: "quickselect"},
    {name: "RUN!", slug: "RUN"},
    {name: "Shield Unequipper", slug: "shield-unequipper"},
    {name: "Signpost Fast Travel", slug: "signpost-fast-travel"},
    {name: "Smart Ammo", slug: "smart-ammo"},
    {name: "Sophie's Skooma Sweetshoppe", slug: "sophies-skooma-sweetshoppe"},
    {name: "Starwind: Immersive Bing's Races", slug: "starwind-immersive-bings-races"},
    {name: "UI modes", slug: "ui-modes"},
    {name: "umo", slug: "umo"},
    {name: "Westly's Playable Dremora and Princess Stompers Gatana's Companion Modernized", slug: "westlys-playable-dremora-and-princess-stompers-gatanas-companion-modernized"},
    {name: "Zack's Lua Multimark", slug: "zacks-lua-multimark"},
];

function getUpdates() {
    const tags = [];
    for (const mod of trackedMods) {
        let org = "modding-openmw";
        if (mod.org)
            org = mod.org;
        let url;
        if (updateListTable) {
            url = `${gitlabAPIURL}/projects/${org}%2F${mod.slug}/repository/tags?per_page=1`;
        } else if (devListTable) {
            url = `${gitlabAPIURL}/projects/${org}%2F${mod.slug}/repository/commits?per_page=1`;
            if (mod.devBranch)
                url += `&ref_name=${mod.devBranch}`;
        }
        const req = new Request(url);
        fetch(req)
            .then(function(response) {
                if (!response.ok) {
                    errors.textContent = `There was an error fetching tags for ${mod.slug}!`;
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then(function(response) {
                //TODO: need error handling here for when we add something without a release yet
                response[0].modName = mod.name;
                response[0].slug = mod.slug;
                if (mod.url !== null)
                    if ((devListTable) && (mod.devURL != undefined)) {
                        response[0].url = mod.devURL;
                    } else {
                        response[0].url = mod.url;
                    }
                if (updateListTable) {
                    response[0].date = new Date(response[0]["commit"].created_at);
                } else if (devListTable) {
                    response[0].date = new Date(response[0].created_at);
                }
                tags.push(response[0]);
                if (tags.length === trackedMods.length)
                    sortAndDisplay(tags);
            });
    }
}

function sortAndDisplay(tags) {
    //https://stackoverflow.com/a/10124053
    for (const tag of tags.sort(function(a, b) {
        return new Date(b.date) - new Date(a.date);
    })) {
        let newTableRow = document.createElement("tr");
        let newTableDataDate = document.createElement("td");
        let newTableDataMod = document.createElement("td");
        let newTableDataLatest = document.createElement("td");
        let newLink = document.createElement("a");
        let newSpanDate = document.createElement("span");
        let newSpanModName = document.createElement("span");
        let newSpanVersion = document.createElement("span");
        let year = tag.date.toISOString().slice(0, 4);
        let date = tag.date.toDateString().slice(4, 10);

        newSpanDate.textContent = `${date}, ${year}`;

        newSpanModName.classList.add("bold");
        newSpanModName.textContent = tag.modName;

        newSpanVersion.classList.add("bold");
        newSpanVersion.textContent = tag.name;

        if (tag.url === undefined) {
            newLink.href = `https://modding-openmw.gitlab.io/${tag.slug}/`;
        } else {
            newLink.href = `https://${tag.url}`;
        }

        newLink.appendChild(newSpanModName);

        newTableDataDate.appendChild(newSpanDate);
        newTableDataMod.appendChild(newLink);
        if (updateListTable)
            newTableDataLatest.appendChild(newSpanVersion);

        newTableRow.classList.add("listicle");
        newTableRow.appendChild(newTableDataDate);
        newTableRow.appendChild(newTableDataMod);
        if (updateListTable)
            newTableRow.appendChild(newTableDataLatest);

        if (updateListTable) {
            updateListTable.appendChild(newTableRow);
        } else if (devListTable) {
            devListTable.appendChild(newTableRow);
        }
    }
}

window.onload = getUpdates();
