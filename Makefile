all: build local-web

build: clean
	./build.sh

debug: clean
	./build.sh --debug

verbose: clean
	./build.sh --verbose

clean:
	rm -rf build site/*.md sha256sums soupault-*-linux-x86_64.tar.gz

local-web:
	python3 -m http.server -d build
